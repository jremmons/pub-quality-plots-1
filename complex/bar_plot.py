#!/usr/bin/python
import numpy as np
import matplotlib as mpl
from matplotlib.patches import Rectangle

# set these constants as the name of the input or output file
# can script this code easily using command line inputs 
OUTPUT_FILE = 'output'
INPUT_FILE = 'data.dat'

# these values specify where the graph will be cut
YMAX_UPPER = 250000
YMIN_UPPER = 200000
YMAX_LOWER = 50000
YMIN_LOWER = 0

# how much space the tic marks wil have from the cuttoff points
TIC_MARK_SPACE = 10000

def figsize(scale):
    # Get this from LaTeX using \the\textwidth
    # will make the plot and fonts the right size
    ########################
    fig_width_pt = 469.755 #
    ########################
    inches_per_pt = 1.0/72.27                   
    aspect_ratio = .325 # ratio of x-dimension to y-dimension
    fig_width = fig_width_pt*inches_per_pt*scale
    fig_height = fig_width*aspect_ratio          
    fig_size = [fig_width,fig_height]
    return fig_size

pgf_with_latex = {
    "pgf.texsystem": "pdflatex",
    "text.usetex": True,  
    "font.family": "serif",
    "axes.labelsize": 10, 
    "text.fontsize": 12,
    "legend.fontsize": 12,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10,
    "figure.figsize": figsize(0.9)
}
mpl.use('pgf')
mpl.rcParams.update(pgf_with_latex)

import matplotlib.pyplot as plt

"""
open files and read in data
"""
f = open(INPUT_FILE, 'r')
f_data = f.read().strip('\n').split('\n')[1:]

"""
Store the data that will be plotted in the upper chart
"""
shd_data_upper = []
seqan_data_upper = []
swps_data_upper = []
af_data_upper = []
for line in f_data:
    times = line.split('\t')
    shd_data_upper.append(float(times[1]))
    seqan_data_upper.append(float(times[0]))
    swps_data_upper.append(float(times[2]))
    af_data_upper.append(float(times[3]))

"""
Store the data that will plotted in the lower chart
The data is bounder by YMAX_LOWER so it doesn't overflow the chart
"""
shd_data_lower = [ min(float(YMAX_LOWER), shd_data_upper[i]) for i in range(len(shd_data_upper)) ]
seqan_data_lower = [ min(float(YMAX_LOWER), seqan_data_upper[i]) for i in range(len(seqan_data_upper)) ]
swps_data_lower = [ min(float(YMAX_LOWER), swps_data_upper[i]) for i in range(len(swps_data_upper)) ]
af_data_lower = [ min(float(YMAX_LOWER), af_data_upper[i]) for i in range(len(af_data_upper)) ]

""" 
Set information for the bar plot and create the rectangles
"""
N = len(shd_data_lower)
ind = np.arange(N) # the x locations for the groups
width = 1.0 / (5) # the width of the bars

"""
Create the figure and the two sets of chart
"""
fig, (ax_upper, ax_lower) = plt.subplots(2,1,sharex=True)

"""
Adjust the plot's borders
"""
fig.subplots_adjust(bottom=.28, top=.85, right=.99, left=.125)

"""
Plot the lower chart data (the rects are stored for use in the legend)
"""
rects1 = ax_lower.bar(ind, shd_data_lower, width, color='black')
rects2 = ax_lower.bar(ind+width, seqan_data_lower, width, color='r', hatch='///')
rects3 = ax_lower.bar(ind+2*width, af_data_lower, width, color='#33CC33', hatch='\\\\\\')
rects4 = ax_lower.bar(ind+3*width, swps_data_lower, width, color='#6666FF', hatch='---')

"""
Plot the upper chart data
"""
ax_upper.bar(ind, shd_data_upper, width, color='black')
ax_upper.bar(ind+width, seqan_data_upper, width, color='r', hatch='///')
ax_upper.bar(ind+2*width, af_data_upper, width, color='#33CC33', hatch='\\\\\\')
ax_upper.bar(ind+3*width, swps_data_upper, width, color='#6666FF', hatch='---')

"""
Set the only ylimits and ytick marks
"""
ax_lower.set_ylim(YMIN_LOWER,YMAX_LOWER)
ax_upper.set_ylim(YMIN_UPPER, YMAX_UPPER)

ax_lower.set_yticks( (YMIN_LOWER, YMAX_LOWER - TIC_MARK_SPACE) )
ax_upper.set_yticks( (YMIN_UPPER + TIC_MARK_SPACE, YMAX_UPPER) )

"""
Remove the borders from the bottom of the upper chart
and the top of the lower chart
"""
ax_upper.spines['bottom'].set_visible(False)
ax_lower.spines['top'].set_visible(False)
ax_upper.xaxis.tick_top()
ax_upper.tick_params(labeltop='off') # don't put tick labels at the top
ax_lower.xaxis.tick_bottom()

"""
Set the slash marks to indicate a jump in the scale
"""
d=0.015
kwargs = dict(transform=ax_upper.transAxes, color='k', clip_on=False)
ax_upper.plot((-d,+d),(-d,+d), **kwargs)
ax_upper.plot((1-d,1+d),(-d,+d), **kwargs)

kwargs.update(transform=ax_lower.transAxes)
ax_lower.plot((-d,+d),(1-d,1+d), **kwargs)
ax_lower.plot((1-d,1+d),(1-d,1+d), **kwargs)

"""
Set the xlimit
"""
num_data_bars = 4 # number of bars per tick mark
plt.xlim([-width,width*((num_data_bars + 1)*len(shd_data_upper))])

"""                                                                      
Set the x/y labels and the title                                         
"""
ax_lower.set_xlabel('Benchmark')
ax_upper.set_ylabel("Runtime (sec)~~~~~~~~~~~~~") # the poor man's way of centering the text

ax_upper.set_title("DNA Read Mapper Comparison")

"""
Set the xlabel and rotate tick marks 45 degrees
"""
plt.xticks(rotation=-45)
ax_lower.set_xticks(ind+3*width)
ax_lower.set_xticklabels( \
    ('B11', 'B12', 'C11', 'C12', 'D11', 'D12', 'E11', 'E12', 'F11', 'F12') )

"""
Plot and set legend
"""
box = ax_upper.get_position()
ax_upper.set_position([box.x0, box.y0, box.width * 0.775, box.height])
ax_lower.set_position([box.x0, box.y0 - box.height -.05, box.width * 0.775, box.height])

ax_upper.legend( (rects1[0], rects2[0], rects3[0], rects4[0]), \
    ('SHD', 'Seqan', 'AF', 'Swps'), loc='upper left', title="Mapper", \
    bbox_to_anchor=(1, 1.3))

"""
Output the pdflatex compiled pdf documents
"""
plt.savefig('{}.pdf'.format(OUTPUT_FILE))
