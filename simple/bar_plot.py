#!/usr/bin/python
import numpy as np
import matplotlib as mpl
from matplotlib.patches import Rectangle

# set these constants as the name of the input or output file
# can script this code easily using command line inputs 
OUTPUT_FILE = "output"
INPUT_FILE = "data.dat"

def figsize(scale):
    # Get this from LaTeX using \the\textwidth
    # will make the plot and fonts the right size
    ########################
    fig_width_pt = 469.755 #
    ########################
    inches_per_pt = 1.0/72.27                   
    aspect_ratio = .325 # ratio of x-dimension to y-dimension
    fig_width = fig_width_pt*inches_per_pt*scale
    fig_height = fig_width*aspect_ratio          
    fig_size = [fig_width,fig_height]
    return fig_size

pgf_with_latex = {
    "pgf.texsystem": "pdflatex",
    "text.usetex": True,  
    "font.family": "serif",
    "axes.labelsize": 10, 
    "text.fontsize": 12,
    "legend.fontsize": 12,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10,
    "figure.figsize": figsize(0.9)
}
mpl.use('pgf')
mpl.rcParams.update(pgf_with_latex)

import matplotlib.pyplot as plt

"""
Open files and read in data
"""
f = open(INPUT_FILE, 'r')
f_data = f.read().strip('\n').split('\n')[1:]

bitvec = []
af = []
for line in f_data:
    times = line.split('\t')
    bitvec.append(float(times[0]))
    af.append(float(times[1]))

""" 
Get information for the bar plot and create the rectangles
"""
N = len(bitvec)
ind = np.arange(N) # the x locations for the groups
width = 1.0 / (3) # the width of the bars

"""
Create the figure and the two sets of chart
"""
fig, ax = plt.subplots(1,1)

"""
Adjust the plot's borders
"""
fig.subplots_adjust(bottom=0.36, top=.85, right=.99, left=.125)

"""
Plot the chart data (the rects are stored for use in the legend)
"""
rects1 = ax.bar(ind, bitvec, width, color='black')
rects2 = ax.bar(ind+width, af, width, color='#33CC33', hatch='\\\\\\')

"""
Set the xlimit (we leave the ylimit to be set automatically)
"""
plt.xlim([-width,width*(3*len(bitvec))])

"""
Set the x/y labels and the title
"""
ax.set_xlabel('Benchmarks')
ax.set_ylabel("False Positive Rate (%)")

ax.set_title("DNA Read Mapper Comparison")

"""
Set the xtick marks and rotate 45 degrees
"""
plt.xticks(rotation=-45)
ax.set_xticks(ind+2*width)
ax.set_xticklabels( \
    ('B11', 'B12', 'C11', 'C12', 'D11', 'D12', 'E11', 'E12', 'F11', 'F12') )

"""
Set the only 3 ytick marks (scale is a percent so out 0%, 25%, and 50%)
"""
ax.set_yticks( (0, 25, 50) )

"""
Plot and set legends
"""
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.775, box.height])
legend = ax.legend( (rects1[0], rects2[0]), ('SHD', 'AF'), loc='upper left', title="Mapper", bbox_to_anchor=(1, 1))

"""
Output the pdflatex compiled pdf documents
"""
plt.savefig('{}.pdf'.format(OUTPUT_FILE))
